package com.vx.example.util;

import com.vx.example.model.DataContainer;

import java.util.List;

public class MathUtils {

    /**
     * Вычисляем массив макс и мин значений для массива параметров
     * @param coords
     * @return
     */
    public static MinMax[] calcMinMax(List<? extends DataContainer> coords) {
        if (coords == null || coords.isEmpty()) {
            return new MinMax[0];
        }
        int dimension = coords.get(0).getData().length;
        MinMax[] minMaxArr = new MinMax[dimension];

        for (int i = 0; i < dimension; i++) {
            MinMax minMax = new MinMax(1.0, 0.0);
            for (DataContainer coord : coords) {
                double value = coord.getData()[i];
                if ( value > minMax.getMax()) {
                    minMax.setMax(value);
                }
                if (value < minMax.getMin()) {
                    minMax.setMin(value);
                }
            }
            minMaxArr[i] = minMax;
        }
        return minMaxArr;
    }

    /**
     * Нормализация массива параметров по формуле x = (xCurrent - xMin) / (xMax - xMin)
     * @param coords
     */
    public static void normalize(List<? extends DataContainer> coords) {
        if (coords == null || coords.isEmpty()) {
            return;
        }
        int dimension = coords.get(0).getData().length;
        MinMax[] minMaxArr = calcMinMax(coords);
        for (DataContainer coord : coords) {
            double[] values = coord.getData();
            for (int i = 0; i < dimension; i++) {
                if (minMaxArr[i].getMin() == minMaxArr[i].getMax() && minMaxArr[i].getMin() == 0) {
                    continue;
                }
                values[i] = (values[i] - minMaxArr[i].getMin())/(minMaxArr[i].getMax() - minMaxArr[i].getMin());
            }
        }
    }



}
