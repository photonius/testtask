package com.vx.example.util;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Используется для вычисления максимальных и минимальных значений какого либо одного параметра для всех объектов
 */
@Data
@AllArgsConstructor
public class MinMax {

    private double min;
    private double max;

    public boolean inRange(double v) {
        return ((v >= min) && (v <= max));
    }
}
