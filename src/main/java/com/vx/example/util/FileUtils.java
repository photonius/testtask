package com.vx.example.util;

import com.opencsv.CSVReader;
import com.vx.example.model.kmeans.Star;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static File loadFileFromResource(String fileName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();;
        return new File(classLoader.getResource(fileName).getFile());
    }

    public static Star[] loadObjectsFromCsv(String csvFile) {
        CSVReader reader = null;
        List<Star> stars = new ArrayList<>();
        try {
            reader = new CSVReader(new FileReader(FileUtils.loadFileFromResource(csvFile)));
            reader.readNext();
            String[] line;
            while ((line = reader.readNext()) != null) {
                double[] data = new double[line.length -1];
                for (int i = 1; i < line.length; i++) {//начинаем с 1-го элемента, пропускаем имя в нулевом столбце
                    if (StringUtils.isNotEmpty(line[i])) {
                        data[i - 1] = Double.parseDouble(line[i].replace(",", "."));
                    } else {
                        data[i - 1] = 0.0;
                    }
                }
                Star star = new Star(line[0], data);
                stars.add(star);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        MathUtils.normalize(stars);
        return stars.stream().toArray(Star[]::new);
    }


}
