package com.vx.example;


import com.vx.example.model.kmeans.Cluster;
import com.vx.example.model.kmeans.Star;
import com.vx.example.util.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Для определения принадлежности игроков к уже определенной группе игроков также используется кластеризация
 * Создается кластер на основе известных сгруппированных игроков и определяется максимальное расстояние
 * Для каждого нового игрока определяется его расстояние до центроида и сравнивается с максимальным расстоянием
 * По итогу сравнения делается вывод о принадлежности нового игрока к кластеру (группе)
 */
@Slf4j
public class Task3 {

    private final static String GROUPPED_PLAYERS_CSV = "groupped_players.csv";

    public static void main(String[] args) {
        new Task3().start();
    }

    private void start() {
        startTask3Alg1();
        //startTask3Alg2();
    }

    private void startTask3Alg1() {//алгоритм на основе кластера
        log.info("");
        log.info("TASK 3 - Group Style By Stats, Algorhytm 1");
        int dimension = 127;
        Star[] stars = FileUtils.loadObjectsFromCsv(GROUPPED_PLAYERS_CSV);
        Star[] newStars = Arrays.copyOfRange(stars, stars.length - 4, stars.length);
        for (Star newStar :  newStars) {
            stars = ArrayUtils.removeElement(stars, newStar);
        }

        Cluster cluster = new Cluster(dimension);
        cluster.setStars(Arrays.asList(stars));
        cluster.recalculateCentroid();
        List<Star> belongToMainGroup = new ArrayList<>();
        List<Star> notBelongToMainGroup = new ArrayList<>();
        for (Star newStar :  newStars) {
            if (cluster.isObjectBelongToClusterAlg1(newStar)) {
                belongToMainGroup.add(newStar);
            } else {
                notBelongToMainGroup.add(newStar);
            }
        }
        log.info("BELONG TO MAIN GROUP:");
        belongToMainGroup.stream().forEach(x -> log.info(x.toString()));
        log.info("NOT BELONG TO MAIN GROUP:");
        notBelongToMainGroup.stream().forEach(x -> log.info(x.toString()));
    }

    /*private void startTask3Alg2() {//алогоритм сравнения по отдельным столбцам - не актуально
        log.info("");
        log.info("TASK 3 - Group Style By Stats, Algorhytm 2");
        int dimension = 127;
        Star[] stars = FileUtils.loadObjectsFromCsv(GROUPPED_PLAYERS_CSV);
        Star[] newStars = FileUtils.loadObjectsFromCsv(NEW_PLAYERS_CSV);


        Cluster cluster = new Cluster(dimension);
        cluster.setStars(Arrays.asList(stars));

        List<Star> belongToMainGroup = new ArrayList<>();
        List<Star> notBelongToMainGroup = new ArrayList<>();
        for (Star newStar :  newStars) {
            if (cluster.isObjectBelongToClusterAlg2(newStar)) {
                belongToMainGroup.add(newStar);
            } else {
                notBelongToMainGroup.add(newStar);
            }
        }
        log.info("BELONG TO MAIN GROUP:");
        belongToMainGroup.stream().forEach(x -> log.info(x.toString()));
        log.info("NOT BELONG TO MAIN GROUP:");
        notBelongToMainGroup.stream().forEach(x -> log.info(x.toString()));
    }*/

}
