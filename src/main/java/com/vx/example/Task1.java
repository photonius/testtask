package com.vx.example;


import com.opencsv.CSVReader;
import com.vx.example.model.shop.Notebook;
import com.vx.example.model.shop.Provider;
import com.vx.example.util.FileUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Магазин канцтоваров
 * Для поиска оптимального поставщика используется следующий алгоритм:
 * вычисляется стоймость одного листа для каждой тетради
 * и умножается на коэффициент качества, высчитываемый на основе первых трех параметров:
 * шероховатость (больше -> хуже), плотность (больше -> лучше), качество обложки(больше -> лучше)
 * высчитается среднее арифметическое по всем тетрадям для каждого поставщика, на основе которого делается вывод о
 * наилучшей стоймости среди всех поставщиков
 */
@Slf4j
public class Task1 {

    public static void main(String[] args) {
        new Task1().start();
    }
    private final static String DATA_CSV = "shop.csv";

    private void start() {
        List<Provider> providers = loadObjectsFromCsv(DATA_CSV);//подгружаем данные из csv файла
        for (Provider provider : providers) {
            log.info("Provider --> {}", provider.getName());
            provider.calcProfitRatio();//считаем коэфициент прибыльности для поставщика (больше - лучше)
            log.info("<-- Provider {}, avgProfitRatio: {}", provider.getName(), provider.getProfitRatio());
        }
        log.info("Best provider: {}", providers.stream().max(Comparator.comparing(Provider::getProfitRatio)).get().getName());
    }

    private List<Provider> loadObjectsFromCsv(String csvFileName) {
        CSVReader reader = null;
        Map<String, Provider> providers = new HashMap<>();//мапа имя_поставщика:поставщик
        try {
            reader = new CSVReader(new FileReader(FileUtils.loadFileFromResource(csvFileName)));
            reader.readNext();
            String[] line;
            while ((line = reader.readNext()) != null) {
                Notebook notebook = new Notebook();
                List<Double> params = new ArrayList<>();
                for (int i = 1; i < 4; i++) {//первые 3 столбца с параметрами грузим в массив
                    params.add(Double.valueOf(line[i]));
                }
                notebook.setData(params.stream().mapToDouble(x -> x).toArray());
                notebook.setPagesCnt(Integer.parseInt(line[4]));//кол-во страниц
                notebook.setCost(Double.parseDouble(line[5]));//цена

                String providerName = line[6];//имя поставщика
                Provider provider = providers.computeIfAbsent(providerName, x -> new Provider(providerName));
                provider.getNotebooks().add(notebook);//распределяем тетрадь, помещая ее в объект соответствующего поставщика
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>(providers.values());
    }

}
