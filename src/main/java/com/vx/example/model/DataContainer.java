package com.vx.example.model;

/**
 * Интерфейс, говорящий о том, что его имплементатор хранит какие либо данные, требующие нормализации в будущем
 */
public interface DataContainer {

    double[] getData();
}
