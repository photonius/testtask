package com.vx.example.model.shop;

import com.vx.example.util.MathUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Поставщик товаров для магазина
 */
@Data
public class Provider {

    private String name;
    private List<Notebook> notebooks = new ArrayList<>();
    private double profitRatio;

    public Provider(String name) {
        this.name = name;
    }

    public void calcProfitRatio() {//подсчет коэфициента прибыльности поставщика (больше - лучше)
        MathUtils.normalize(notebooks);//нормализуем значения первых трех столбцов (кроме цены и кол-ва страниц)
        profitRatio = 0.0;
        for (Notebook notebook : notebooks) {
            double noteBookRatio = notebook.calcProfitRatio();//подсчет коэфициента прибыльности конкретной тетради (больше - лучше)
            profitRatio = profitRatio + noteBookRatio;
        }
        profitRatio = profitRatio / notebooks.size();
    }
}
