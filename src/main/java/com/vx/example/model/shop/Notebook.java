package com.vx.example.model.shop;

import com.vx.example.model.DataContainer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Тетрадь, которую поставляет поставщик
 */
@Data
@Slf4j
public class Notebook implements DataContainer {

    private double cost;
    private int pagesCnt;

    private double[] data;//шероховатость, плотность и качество хранятся в массиве, данные параметры будут нормализоваться

    public double calcProfitRatio() {
        double sum = (1 - data[0])//чем больше шероховатость, тем хуже, поэтому инвертируем параметр
                + data[1] + data[2];//суммируем параметры после нормализации
        //предполагается, что эти коэфициенты имеют одинаковый вес для потребителя
        double qualityRatio = sum / 3;//считаем среднее значение
        double onePageCost = (cost / pagesCnt);//цена одной страницы
        double profitRatio = qualityRatio / onePageCost;//коэфициент качества / цена страницы
        // чем больше коэф. качества, тем больше коэф. прибыли
        // цена в знаменателе, т.к. чем больше цена, тем меньше коэф. прибыли
        log.info("Notebook with pages cnt = {}, one page cost = {}, profitRatio = {}", pagesCnt, onePageCost, profitRatio);
        return profitRatio;
    }
}
