package com.vx.example.model.kmeans;

/**
 * Абстрактный класс, содержащий методы вычисления расстояний
 */
public abstract class Space {

    protected int dimension = 0;

    protected double calcDist(double[] p1, double[] p2) {//вычисление расстояниея между объектами
        double dist = 0;
        for (int i = 0; i < dimension; i++) {
            dist += (p1[i] - p2[i]) * (p1[i] - p2[i]);
        }
        return Math.sqrt(dist);
    }

    protected double calcDistSquare(double[] p1, double[] p2) {//вычисление среднеквадратичного расстояния между объектами
        double dist = 0;
        for (int i = 0; i < dimension; i++) {
            dist += (p1[i] - p2[i]) * (p1[i] - p2[i]);
        }
        return dist;
    }

}
