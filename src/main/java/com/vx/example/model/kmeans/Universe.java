package com.vx.example.model.kmeans;

import com.vx.example.chart.CostChartDrawer;
import com.vx.example.chart.UniverseChartDrawer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

/**
 * Объект Вселенной, имеющий набор кластеров
 */
@Data
@Slf4j
public class Universe extends Space {

    private int delay = 0;

    private int clusterCnt;
    private final UniverseChartDrawer chartDrawer;
    private final CostChartDrawer costChartDrawer;

    private Cluster[] clusters;
    private Star[] allStars;//изначальные, не сортированные по кластерам, объекты
    private List<Double> costList;
    private int maxCentroidRecalcTries = 100;
    private int maxClusters = 20;
    private boolean show2DGraph;

    public Universe(int maxCentroidRecalcTries, int maxClusters, int dimension) {
        this.chartDrawer = new UniverseChartDrawer();
        this.costChartDrawer = new CostChartDrawer();
        this.maxCentroidRecalcTries = maxCentroidRecalcTries;
        this.maxClusters = maxClusters;
        costList = new ArrayList<>();
        this.dimension = dimension;
    }

    public Universe(double[][] data, int maxCentroidRecalcTries, int maxClusters, int dimension) {
        this.dimension = dimension;
        this.chartDrawer = new UniverseChartDrawer();
        this.costChartDrawer = new CostChartDrawer();
        this.maxCentroidRecalcTries = maxCentroidRecalcTries;
        this.maxClusters = maxClusters;
        costList = new ArrayList<>();
        allStars = new Star[data.length];

        for (int j = 0; j < data.length; j++) {
            allStars[j] = new Star(data[j]);//инициализируем объекты переданными данными
        }
        //MathUtils.normalize(dimension, Arrays.asList(allStars));
    }

    /**
     * Старт кластеризации
     */
    public void calc() {
        log.info("Started clusterizing...");
        log.info("MAX_CLUSTERS = {}", maxClusters);
        log.info("OBJECTS = {}", allStars.length);
        for (int k = 1; k <= maxClusters; k++) {//сначала начинаем с 1 кластера и доходим до заданного значения. нужно для построения графика ценности
            log.info("");
            log.info("Trying number of clusters = {}", k);
            if (k > allStars.length) {
                break;
            }
            this.clusterCnt = k;
            this.clusters = new Cluster[clusterCnt];
            clusterize();//запуск разбиения на кластеры
        }

        log.info("Finished clusterizing...");
        outputClusters();//выводим результат в лог
        log.info("Finished...");
        costChartDrawer.drawChart(costList);
    }

    private void outputClusters() {
        for (Cluster cluster : clusters) {
            log.info(cluster.toString());
        }
        log.info("");
        log.info("SUMMARY:");
        int cnt = 1;
        for (Cluster cluster : clusters) {
            log.info("");
            log.info("Cluster #{}, objects = {}", cnt, cluster.getStars().size());
            StringBuilder sb = new StringBuilder();
            cluster.getStars().stream().forEach(x -> sb.append(x.getName()).append(", "));
            log.info(sb.toString());
            cnt++;
        }
    }

    /**
     * Основной метод кластеризации
     */
    private void clusterize() {
        initClusters();//формируем начальные центроиды, они равны первым элементам
        log.info("MAX_TRIES_FOR_CALCULATE_CENTROIDS = {}", maxCentroidRecalcTries);
        for (int i = 0; i < maxCentroidRecalcTries; i++) {
            log.info("Centroids calculation try: {}", i + 1);
            clearClustersStars();//очищаем данные об объектах в кластере
            sortStarsToClusters();//распределяем звезды по кластерам
            drawChart(i);//рисуем график (если включено)
            recalculateCentroids();//пересчитывем центроиды
            if (delay > 0) {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (!clustersCentroidsChanged()) {//если ни один центроид не изменился => выход
                break;
            }
        }
        costList.add(calcCost());//считаем ценность
    }

    private void drawChart(int iterationNum) {
        if (show2DGraph) {
            chartDrawer.drawChart(this, iterationNum + 1);
        }
    }

    private double calcCost() {//считаем ценность, как среднеквадратичное расстояние элементов до центроида
        double cost = 0.0;
        for (Cluster cluster : clusters) {
            for (Star star : cluster.getStars()) {
                cost += calcDistSquare(cluster.getCentroid(), star.getData());
            }
        }
        return cost;
    }

    private boolean clustersCentroidsChanged() {
        for (Cluster cluster : clusters) {
            if (cluster.isCentroidChanged()) {
                return true;
            }
        }
        return false;
    }

    private void clearClustersStars() {
        Arrays.stream(clusters).forEach(Cluster::clearStars);
    }

    private void recalculateCentroids() {
        Arrays.stream(clusters).forEach(Cluster::recalculateCentroid);
    }

    private void sortStarsToClusters() {
        for (Star star : allStars) {
            TreeMap<Double, Cluster> distMap = new TreeMap<Double, Cluster>();//используем для сортировки по расстоянию
            for (Cluster cluster : clusters) {//считаем расстояние от каждого центроида до объекта
                double dist = calcDist(cluster.getCentroid(), star.getData());
                distMap.put(dist, cluster);
            }
            distMap.firstEntry().getValue().addStar(star);//добавляем объект в кластер, растояние до центроида которого минимально
        }
    }

    private void initClusters() {
        for (int i = 0; i < clusterCnt; i++) {
            Cluster cluster = new Cluster(dimension);
            cluster.setCentroid(allStars[i].getData());
            clusters[i] = cluster;
        }
    }

}
