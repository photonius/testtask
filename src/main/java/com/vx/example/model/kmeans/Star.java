package com.vx.example.model.kmeans;

import com.vx.example.model.DataContainer;
import lombok.Data;
import org.apache.commons.lang3.text.StrBuilder;

/**
 * Объект в n-мерном пространстве, имеющий n-координат (параметров)
 */
@Data
public class Star implements DataContainer {

    private double[] data;
    private String name;

    public Star(double[] data) {
        this.data = data;
    }

    public Star(String name, double[] data) {
        this.name = name;
        this.data = data;
    }

    @Override
    public String toString() {
        StrBuilder sb = new StrBuilder();
        sb.append("Star: " + name);
        //Arrays.stream(data).forEach(x -> sb.append(x).append(", "));
        //sb.appendNewLine();
        return sb.toString();
    }
}
