package com.vx.example.model.kmeans;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.text.StrBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@Slf4j
public class Cluster extends Space {

    private double[] centroid;
    private double[] prevCentroid;
    private List<Star> stars = new ArrayList<Star>();

    public Cluster(int dimension) {
        this.dimension = dimension;
        centroid = new double[dimension];
    }

    public void addStar(Star star) {
        stars.add(star);
    }

    public void recalculateCentroid() {//пересчитываем центроиды
        prevCentroid = centroid.clone();
        for (int i = 0; i < dimension; i++) {
            centroid[i] = average(stars, i);
        }
    }

    private double average(List<Star> stars, int i) {//ищем среднеарифметическое по каждому измерению для центроида
        double sum = stars.stream().mapToDouble(x -> x.getData()[i]).sum();
        return sum / stars.size();
    }

    public void clearStars() {
        stars.clear();
    }

    public void setCentroid(double[] data) {
        centroid = data.clone();
    }

    public boolean isCentroidChanged() {
        for (int i = 0; i < dimension; i++) {
            if (centroid[i] != prevCentroid[i]) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StrBuilder sb = new StrBuilder();
        sb.append("Centroid: ");
        Arrays.stream(centroid).forEach(x -> sb.append(x).append(", "));
        sb.appendNewLine();
        stars.stream().forEach(x -> sb.append(x.toString()));
        sb.appendNewLine();
        return sb.toString();
    }

    public boolean isObjectBelongToClusterAlg1(Star newStar) {//проверяем, входит ли объект в кластер
        log.info("{} - Check that object belongs to main group", newStar.getName());
        //максимальное расстояние до центроида
        double maxDist = stars.stream().mapToDouble(x -> calcDist(centroid, x.getData())).max().getAsDouble();
        //расстояние до объекта
        double dist = calcDist(centroid, newStar.getData());
        log.info("MaxDist = {}, currentDist = {}, result = {}", maxDist, dist, dist <= maxDist);
        return dist <= maxDist;
    }

    //не используется
    /*public boolean isObjectBelongToClusterAlg2(Star newStar) {//TODO: should be optimized. do not calc maxDist every time
        log.info("{} - Check that object belongs to main group", newStar.getName());
        MinMax[] minMaxArr = MathUtils.calcMinMax(stars);

        for (int i = 0; i < dimension; i++) {
            double value = newStar.getData()[i];
            log.info("index = {}, value = {}, min = {}, max = {}", i, value, minMaxArr[i].getMin(), minMaxArr[i].getMax());
            if (!minMaxArr[i].inRange(value)) {
                log.info("NOT BELONG...");
                return false;
            }
        }
        log.info("BELONG...");
        return true;
    }*/


}
