package com.vx.example;


import com.vx.example.model.kmeans.Universe;
import com.vx.example.util.FileUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Для группировки игроков по признакам используется алгоритм кластеризации k-means
 * Число кластеров подбирается экспериментально на основе графика ценности по методу elbow,
 * который выводится в конце работы алгоритма
 * Для тестирования алогоритма для 2-х параметров можно запустить KmeansTest, где будет
 * визуально показываться процесс работы кластеризации
 */
@Slf4j
public class Task2 {

    private final static String DATA_CSV = "players.csv";

    public static void main(String[] args) {
        new Task2().start();
    }

    private void start() {
        startTask2();
    }

    private void startTask2() {
        int maxCentroidRecalcTries = 100;//максимальное кол-во попыток пересчитать центроид
        int maxClusters = 7;//число кластеров, выясняется экспериментально после построения графика ценности
        int dimension = 127;//число измерений = равно числу параметров

        log.info("TASK 2 - Player Groups");

        Universe universe = new Universe(maxCentroidRecalcTries, maxClusters, dimension);
        universe.setAllStars(FileUtils.loadObjectsFromCsv(DATA_CSV));
        //universe.setShow2DGraph(true);
        universe.calc();
    }

}
