package com.vx.example.chart;

import org.knowm.xchart.*;

import java.util.List;

public class CostChartDrawer {

    private SwingWrapper<XYChart> swingWrapper;

    public CostChartDrawer() {
        //chart = new XYChartBuilder().width(1500).height(1000).title("Cost").xAxisTitle("X").yAxisTitle("Y").build();
    }

    public void drawChart(List<Double> data) {
        int size = data.size();
        double[] xData = new double[size];
        double[] yData = new double[size];
        for (int i = 0; i < size; i++) {
            xData[i] = i + 1;
            yData[i] = data.get(i);
        }
        XYChart chart = QuickChart.getChart("Cost Chart", "K", "J", "J(K)", xData, yData);
        new SwingWrapper(chart).displayChart();
    }

}
