package com.vx.example.chart;

import com.vx.example.model.kmeans.Cluster;
import com.vx.example.model.kmeans.Star;
import com.vx.example.model.kmeans.Universe;
import org.knowm.xchart.BubbleChart;
import org.knowm.xchart.BubbleChartBuilder;
import org.knowm.xchart.SwingWrapper;

import java.util.List;

public class UniverseChartDrawer {

    private final BubbleChart chart;
    private SwingWrapper<BubbleChart> swingWrapper;

    public UniverseChartDrawer() {
        chart = new BubbleChartBuilder().width(1500).height(1000).title("Universe").xAxisTitle("X").yAxisTitle("Y").build();
    }

    public void drawChart(Universe universe, int iterationNum) {
        for (int i = 1; i <= universe.getClusters().length; i++) {
            chart.removeSeries(String.valueOf(i));
        }
        BubbleChart chart = getChart(universe);
        chart.setTitle("Iteration: " + iterationNum);
        if (swingWrapper == null) {
            swingWrapper = new SwingWrapper<BubbleChart>(chart);
            swingWrapper.displayChart();
        } else {
            swingWrapper.repaintChart();
        }
    }

    private BubbleChart getChart(Universe universe) {
        int X_INDEX = 0;
        int Y_INDEX = 1;
        int cnt = 1;
        for (Cluster cluster : universe.getClusters()) {
            List<Star> stars = cluster.getStars();
            int size = stars.size() + 1;
            // Series
            double[] xData = new double[size];
            double[] yData = new double[size];
            double[] bubbleData = new double[size];

            for (int num = 0; num < (size - 1); num++) {
                xData[num] = stars.get(num).getData()[X_INDEX];
                yData[num] = stars.get(num).getData()[Y_INDEX];
                bubbleData[num] = 10;
            }
            xData[size - 1] = cluster.getCentroid()[X_INDEX];
            yData[size - 1] = cluster.getCentroid()[Y_INDEX];
            bubbleData[size - 1] = 30;

            chart.addSeries(String.valueOf(cnt), xData, yData, bubbleData);
            cnt++;
        }


        return chart;
    }

}
