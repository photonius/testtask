package com.vx.example;

import com.vx.example.model.kmeans.Universe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KmeansTest {

    //тестовые данные для проверки алгоритма k-means с визуальным 2D отображением
    private double data[][] = { {0.15, 0.17},
            {0.33, 0.78},
            {0.04, 0.09},
            {0.44, 0.13},
            {0.88, 0.75},
            {0.11, 0.17},
            {0.48, 0.37},
            {0.98, 0.34},
            {0.15, 0.47},
            {0.55, 0.37},
            {0.75, 0.07},
            {0.25, 0.25},
            {0.41, 0.50},
            {0.85, 0.97},
            {0.65, 0.17},
            {0.85, 0.10},
            {0.33, 0.44},
            {0.88, 0.24},
            {0.71, 0.30},
            {0.66, 0.40},
    };

    public static void main(String[] args) {
        new KmeansTest().start();
    }

    private void start() {
        startTest();
    }

    private void startTest() {
        int maxCentroidRecalcTries = 100;
        int maxClusters = 10;
        int dimension = 2;
        int delay = 1000;

        log.info("TEST");

        Universe universe = new Universe(data, maxCentroidRecalcTries, maxClusters, dimension);
        universe.setShow2DGraph(true);
        universe.setDelay(delay);
        universe.calc();
    }

}
